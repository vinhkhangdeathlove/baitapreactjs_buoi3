import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { name, description, image } = this.props.shoeData;
    return (
      <div className="col-4">
        <div className="card">
          <img src={image} className="card-img-top" alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{description}</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.shoeData);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

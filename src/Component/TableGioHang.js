import React, { Component } from "react";

export default class TableGioHang extends Component {
  renderContent = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr style={{ lineHeight: "60px" }} key={item.id}>
          <td>{item.id}</td>
          <td>
            <img src={item.image} style={{ width: "60px" }} alt="" />
          </td>
          <td className="col-4">{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-success"
            >
              <i className="fa fa-minus"></i>
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              <i className="fa fa-plus"></i>
            </button>
          </td>
          <td>
            <span className="mx-2">{item.total}</span>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              <i className="fa fa-trash-alt"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContent()}</tbody>
        </table>
      </div>
    );
  }
}

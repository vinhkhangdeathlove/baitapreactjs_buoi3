import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";

export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };

  handleAddToCart = (sp) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index === -1) {
      let newSp = { ...sp, soLuong: 1, total: sp.price };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].soLuong++;
      cloneGioHang[index].total =
        cloneGioHang[index].soLuong * cloneGioHang[index].price;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  handleRemove = (id) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === id;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };

  handleChangeQuantity = (id, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === id;
    });
    let cloneGioHang = [...this.state.gioHang];

    cloneGioHang[index].soLuong += step;
    cloneGioHang[index].total =
      cloneGioHang[index].soLuong * cloneGioHang[index].price;
    if (cloneGioHang[index].soLuong === 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };

  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            gioHang={this.state.gioHang}
            handleChangeQuantity={this.handleChangeQuantity}
            handleRemove={this.handleRemove}
          />
        )}
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          shoeData={this.state.shoeArr}
        />
      </div>
    );
  }
}

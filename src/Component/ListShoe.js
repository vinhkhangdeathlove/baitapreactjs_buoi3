import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderShoes = () => {
    return this.props.shoeData.map((item) => {
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCart}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };
  render() {
    return <div className="row mx-5">{this.renderShoes()}</div>;
  }
}
